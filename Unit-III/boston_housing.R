library("MASS")
library("ISLR")

data(Boston)
summary(Boston)

# Correlation matrix
#pairs(~ medv + crim + zn + indus + chas + nox + rm + age + dis + rad + tax + ptratio + black + lstat, data=Boston, main="Correlacion de atributos de Boston")
#pairs(~ medv + rm + lstat, data=Boston, main="Correlacion de atributos de Boston")

first_model = lm(medv~rm, data=Boston)
summary(first_model)

confint(first_model)
predict(first_model, data.frame(rm=c(10,20,30)), interval="confidence")

second_model = lm(medv~lstat, data=Boston)
summary(second_model)

confint(second_model)
predict(second_model, data.frame(lstat=c(10,20,30)), interval="confidence")
