from copy import deepcopy


DEPTH = -1
INF = float('inf')
MINUS_INF = -float('inf')


def heuristic(node):
    """
        Heuristic take score in function of how well the players perform.
    """

    if node.winner() == 'O':
        return 1
    elif node.winner() == 'X':
        return -1
    return 0


def minimax(node, depth, alpha, beta, is_maximazing):
    """
        Implementation taken from:
        https://en.wikipedia.org/wiki/Alpha%E2%80%93beta_pruning#Pseudocode
    """
    
    if depth == 0 or not node.winner() == '':
        return heuristic(node)

    if is_maximazing:
        value = MINUS_INF

        for spot in node.free_spots():
            child = deepcopy(node)
            child.next_move(*spot)
            value = max(value, minimax(child, depth - 1, alpha, beta, False))
            alpha = max(value, alpha)

            if alpha >= beta:
                break

        return value
    else:
        value = INF

        for spot in node.free_spots():
            child = deepcopy(node)
            child.next_move(*spot)
            value = min(value, minimax(child, depth - 1, alpha, beta, True))
            beta = min(value, beta)

            if alpha >= beta:
                break

        return value


def ai_selection(node):

    best_score = MINUS_INF
    best_spot = tuple()

    for spot in node.free_spots():
        aux = deepcopy(node)
        aux.next_move(*spot)
        new_score = minimax(aux, DEPTH, MINUS_INF, INF, True)

        if new_score > best_score:
            best_score = new_score
            best_spot = spot

    return best_spot
