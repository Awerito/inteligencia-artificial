from tictactoe import Tictactoe as Ttt
from minimax import *


ai = 'O'
game = Ttt()

while not game.winner():

    if game.get_turn() == ai:
        pos = ai_selection(deepcopy(game))
        game.next_move(*pos)
    else:
        pos = tuple(int(x) for x in input("Coordinates: ").split())
        game.next_move(*pos)

    print(game, '\n')

if not game.winner() == 'T':
    print("The winner is", game.winner())
else:
    print("Tie!")
