class Tictactoe:

    def __init__(self, size=3):

        self.size = size
        self.turn = 'X'
        self.done = False
        self.board = [[' ' for _ in range(size)] for _ in range(size)]


    def reset(self): self.__init__(size=self.size)


    def get_board(self): return self.board


    def get_turn(self): return self.turn


    def get_size(self): return self.size


    def free_spots(self):

        spots = []
        for i, row in enumerate(self.board):
            for j, val in enumerate(row):
                if val == ' ':
                    spots.append((i, j))

        return spots


    def next_move(self, i, j):

        try:
            if self.board[i][j] == ' ' and not self.done:
                if self.turn == 'X':
                    # First player
                    self.board[i][j] = 'X'
                    self.turn = 'O'
                else:
                    self.board[i][j] = 'O'
                    self.turn = 'X'

                return True
            return False
        except IndexError:
            return False


    def winner(self):

        # Horizontal
        for row in self.board:
            vals = set(row)
            if len(vals) == 1 and not vals == {' '}:
                return 'X' if vals == {'X'} else 'O'

        # Vertical
        for cols in zip(*self.board): #zip(*self.board) it's eq to a pseudo transpose
            vals = set(cols)
            if len(vals) == 1 and not vals == {' '}:
                return 'X' if vals == {'X'} else 'O'

        # Diagonal
        first_diag = set([self.board[i][i] for i in range(self.size)])
        second_diag = set([self.board[i][self.size - 1 - i] for i in range(self.size)])

        if len(first_diag) == 1 and not first_diag == {' '}:
            return 'X' if first_diag == {'X'} else 'O'

        if len(second_diag) == 1 and not second_diag == {' '}:
            return 'X' if second_diag == {'X'} else 'O'

        list_board = []
        for row in self.board:
            list_board += row

        if not ' ' in set(list_board):
            return 'T'

        return ''


    def __str__(self):
        lines = [' ' + ' | '.join(row) + ' ' for row in self.board]
        separator = '\n' + '_' * len(lines[0]) + '\n'
        return separator.join(lines)


    def __repr__(self):
        lines = [' ' + ' | '.join(row) + ' ' for row in self.board]
        separator = '\n' + '_' * len(lines[0]) + '\n'
        return separator.join(lines)


if __name__=="__main__":

    test = Tictactoe()
    test.next_move(0, 0)
    test.next_move(2, 0)
    test.next_move(1, 2)
    test.next_move(0, 1)
    test.next_move(2, 1)
    test.next_move(2, 2)
    print(test.free_spots())
    print(test)
