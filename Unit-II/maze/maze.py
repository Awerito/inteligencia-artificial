from pygame import draw, Rect
from random import choice


class Block:

    def __init__(self, x, y, size):
        
        self.x, self.y = x, y
        # Walls    = [ Up, Right, Down, Left]
        self.walls = [True, True, True, True]
        self.visited = False
        self.current = False
        self.rectangle = Rect(x, y, size, size)


    def is_visited(self): return self.visited


    def is_current(self): return self.current


    def set_visited(self): self.visited = True


    def set_current(self): self.current = not self.current


class Maze:

    moves = ['up', 'right', 'down', 'left']

    def __init__(self, screen, pivot=[0,0], resolution=[100, 100], size=10):
        
        self.screen = screen
        self.pivot = pivot
        self.resolution = resolution
        self.size = size
        self.maze = [
                [Block(x, y, size) for x in range(0, self.resolution[0], self.size)]
                for y in range(0, self.resolution[1], self.size)
        ]


    def find_neighbors(self, block):
        
        result = []
        x, y = block.x//self.size, block.y//self.size

        if 0 <= x - 1:
            if not self.maze[y][x - 1].is_visited():
                result.append(self.maze[y][x - 1])

        if x + 1 < len(self.maze[0]):
            if not self.maze[y][x + 1].is_visited():
                result.append(self.maze[y][x + 1])

        if 0 <= y - 1:
            if not self.maze[y - 1][x].is_visited():
                result.append(self.maze[y - 1][x])

        if y + 1 < len(self.maze):
            if not self.maze[y + 1][x].is_visited():
                result.append(self.maze[y + 1][x])

        if result:
            return choice(result)
        return None


    def remove_wall(self, block1, block2):

        if block1.y - block2.y == -self.size:
            block1.walls[2] = False
            block2.walls[0] = False

        if block1.y - block2.y == self.size:
            block1.walls[0] = False
            block2.walls[2] = False

        if block1.x - block2.x == -self.size:
            block1.walls[1] = False
            block2.walls[3] = False

        if block1.x - block2.x == self.size:
            block1.walls[3] = False
            block2.walls[1] = False


    def draw_maze(self):
        
        for row in self.maze:
            for block in row:
                color = (43, 48, 58) 

                if block.is_visited():
                    color = (214, 73, 51)

                if block.is_current():
                    color = (88, 164, 176)

                draw.rect(self.screen, color, block.rectangle)

                if block.walls[0]:
                    draw.line(self.screen, (65, 93, 67),
                            (block.x, block.y), (block.x + self.size, block.y))

                if block.walls[1]:
                    draw.line(self.screen, (65, 93, 67),
                            (block.x + self.size - 1, block.y),
                            (block.x + self.size - 1, block.y + self.size))

                if block.walls[2]:
                    draw.line(self.screen, (65, 93, 67),
                            (block.x, block.y + self.size - 1),
                            (block.x + self.size, block.y + self.size - 1))
                    
                if block.walls[3]:
                    draw.line(self.screen, (65, 93, 67),
                            (block.x, block.y), (block.x, block.y + self.size))
