import pygame
from maze import Maze


WIDTH, HEIGHT = 1280, 720
FPS = 60
pygame.init()
screen = pygame.display.set_mode((WIDTH, HEIGHT))
clock = pygame.time.Clock()
done = False

maze = Maze(screen, resolution=[WIDTH, HEIGHT], size=40)
maze_done = False

#Maze initial conditions
current = maze.maze[0][0]
stack = []
#=======================

while not done:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True

    #Maze generation
    current.set_visited()
    current.set_current()
    next_block = maze.find_neighbors(current)

    if next_block:
        next_block.set_visited()
        maze.remove_wall(current, next_block)
        stack.append(current)
        current = next_block
    elif stack:
        current = stack.pop()
        current.set_current()
    else:
        maze_done = True
    #===============

    #Draw Area
    maze.draw_maze()
    #=========

    if not maze_done:
        clock.tick(FPS)
        pygame.display.update()
