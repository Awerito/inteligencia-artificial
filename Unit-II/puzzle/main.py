import sys
from random import choice
from tree_search import search_solution
from puzzle import Puzzle
from tree import Tree


sys.setrecursionlimit(100000)

#Create the 3x3 board game
game = Puzzle()

#Random movements
for _ in range(20): game.move(choice(['up', 'down', 'left', 'right']))
print("Game board:")
print(game)
print('='*24)

#Start the tree of states
init_state = (game.get_board(), 'start')
game_tree = Tree(init_state)

#Expand search tree
final_state = [
         0, 1, 2,
         3, 4, 5,
         6, 7, 8
]
#search_mode = 'DFS' #Do not show the tree, it get messy
search_mode = 'BFS'
solution = search_solution(game_tree, final_state, search_mode)
solution.reverse()
for move in solution:
    print(move)
print('='*24)


#Final tree
game_tree.print_tree()
