from copy import deepcopy
from puzzle import Puzzle
from tree import Tree


def search_solution(tree, final_state, mode):
    """
        Breadth-first search python implementation and Deep-first search python
        implementation.
        Modify version of BFS and DFS for use of Tree expansion. Return a list
        of elements to the root from the search element if is found. Return an empty list
        otherwise.
    """

    moves = ['up', 'down', 'left', 'right']

    history = [tree]
    #state list avoid duplicated values.
    state = [tree.data[0]]
    while history:
        aux_tree = history.pop(0)

        if aux_tree.data[0] == final_state:
            return aux_tree.chain_to_root()

        #Additional code for auto-expansion of the tree.
        for move in moves:
            new_game = Puzzle(board=deepcopy(aux_tree.data[0]))
            new_state = (new_game.move(move, silent=False), move,)
            if not (new_state[0] in state or not new_state[0]):
                state.append(new_state[0])
                tree.add_leaf(new_state, target=aux_tree.data)
        #===============================================

        if mode == 'BFS':
            history = history + aux_tree.leafs
        else:
            history = aux_tree.leafs + history
    return []
