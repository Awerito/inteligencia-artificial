mean_temp_f(santiago, 70).
mean_temp_f(arica, 80).
mean_temp_f(punta_arenas, 30).
mean_temp_f(puerto_montt, 50).

mean_temp_c(CITY, TEMP_C) :-
    mean_temp_f(CITY, TEMP_F),
    TEMP_C is (TEMP_F - 32) / 1.8.
